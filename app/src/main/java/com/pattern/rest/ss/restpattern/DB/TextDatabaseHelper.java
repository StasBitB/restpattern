package com.pattern.rest.ss.restpattern.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Sokol on 8/7/2015.
 */
public class TextDatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "textManager";
    public static final String TABLE_TEXT = "text";
    public static final String KEY_ID = "_id";
    public static final String KEY_TEXT = "name";


    public TextDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_TEXT + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + KEY_TEXT + " TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertText(String text){
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_TEXT,text);
        getWritableDatabase().insert(TABLE_TEXT,null,contentValues);
    }

}
