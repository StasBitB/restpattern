package com.pattern.rest.ss.restpattern.Fragment;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.pattern.rest.ss.restpattern.DB.TextDatabaseHelper;
import com.pattern.rest.ss.restpattern.ListAdapter.CursorListAdapter;
import com.pattern.rest.ss.restpattern.Provider.TextProvider;
import com.pattern.rest.ss.restpattern.R;
import com.pattern.rest.ss.restpattern.Service.SaverService;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoaderActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private SaverService mSaverService;
    private EditText mSaveText;
    private Button mSaveButton;
    private ListView mListView;
    private CursorListAdapter mCursorListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(1, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_loader, container, false);
        mSaveButton = (Button) root.findViewById(R.id.saveButton);
        mSaveText = (EditText) root.findViewById(R.id.saveText);
        mListView = (ListView) root.findViewById(R.id.listView);

        mCursorListAdapter = new CursorListAdapter(getActivity(), null, 0);
        mListView.setAdapter(mCursorListAdapter);

        mSaveButton.setOnClickListener(onClick);

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(getActivity().getBaseContext(), SaverService.class);
        getActivity().startService(intent);
        getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unbindService(serviceConnection);

    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        String[] projection = {TextDatabaseHelper.KEY_ID, TextDatabaseHelper.KEY_TEXT};

        Uri CONTENT_URI = Uri.parse("content://" + TextProvider.AUTHORITY
                + "/" + "text");
        CursorLoader cursorLoader = new CursorLoader(getActivity(),
                CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursorListAdapter.changeCursor(data);
    }

    @Override
    public void onLoaderReset(Loader loader) {
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SaverService.ServiceBinder serviceBinder = (SaverService.ServiceBinder) service;
            mSaverService = serviceBinder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String text = mSaveText.getText().toString();
            mSaverService.saveToDB(text);
        }
    };
}

