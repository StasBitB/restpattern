package com.pattern.rest.ss.restpattern.ListAdapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.pattern.rest.ss.restpattern.DB.TextDatabaseHelper;
import com.pattern.rest.ss.restpattern.R;

/**
 * Created by Sokol on 8/8/2015.
 */
public class CursorListAdapter extends CursorAdapter {


    public CursorListAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView textView = (TextView) view.findViewById(R.id.textView);
        String s = cursor.getString(cursor.getColumnIndexOrThrow(TextDatabaseHelper.KEY_TEXT));
        textView.setText(s);
    }
}
