package com.pattern.rest.ss.restpattern.Provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.pattern.rest.ss.restpattern.DB.TextDatabaseHelper;

/**
 * Created by Sokol on 8/7/2015.
 */
public class TextProvider extends ContentProvider {

    public static final String AUTHORITY = "com.pattern.rest.ss.restpattern.DB";
    private TextDatabaseHelper mTextDatabaseHelper;
    private SQLiteDatabase mSqLiteDatabase;
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI("com.pattern.rest.ss.restpattern.DB", "text", 1);
    }

    @Override
    public boolean onCreate() {
        mTextDatabaseHelper = new TextDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        mSqLiteDatabase = mTextDatabaseHelper.getReadableDatabase();
        Cursor cursor = null;
        switch (sURIMatcher.match(uri)) {
            case 1:
                cursor = mSqLiteDatabase.query(TextDatabaseHelper.TABLE_TEXT,null,null,null,null,null,null);
                break;
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        mSqLiteDatabase = mTextDatabaseHelper.getWritableDatabase();
        switch (sURIMatcher.match(uri)) {
            case 1:
                mSqLiteDatabase.insert(TextDatabaseHelper.TABLE_TEXT,null,values);
                break;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
