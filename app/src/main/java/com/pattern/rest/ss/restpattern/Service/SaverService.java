package com.pattern.rest.ss.restpattern.Service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.*;
import android.widget.Toast;

import com.pattern.rest.ss.restpattern.DB.TextDatabaseHelper;
import com.pattern.rest.ss.restpattern.Provider.TextProvider;


/**
 * Created by Sokol on 8/7/2015.
 */
public class SaverService extends Service {

    private final IBinder binder = new ServiceBinder();
    private LooperHandler mLooperHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "created", Toast.LENGTH_SHORT).show();
        HandlerThread handlerThread = new HandlerThread("MyService",android.os.Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        mLooperHandler = new LooperHandler(handlerThread.getLooper());
    }

    public class ServiceBinder extends Binder {
        public SaverService getService() {
            return SaverService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public void saveToDB(String s){
        Message message = mLooperHandler.obtainMessage();
        message.obj = s;
        mLooperHandler.sendMessage(message);
    }

    private class LooperHandler extends Handler {

        public LooperHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String text = (String)msg.obj;

            Uri CONTENT_URI = Uri.parse("content://" + TextProvider.AUTHORITY
                    + "/" + "text");
            ContentValues contentValues = new ContentValues();
            contentValues.put(TextDatabaseHelper.KEY_TEXT,text);
            getContentResolver().insert(CONTENT_URI, contentValues);

        }
    }
}
